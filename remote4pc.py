import config         # our module
import subprocess
import telegram
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

# define telegram bot
updater = Updater(token=config.token, use_context=True)
dispatcher = updater.dispatcher


def about():
    text = '''
v 0\.1 Aka demo
\- [x] Volume control

All Copyrights Rurek
Ps Kocham Cie Rurku
 '''
    return text


# authnetication function
def auth(update, context):
    if str(update.effective_chat.id) not in config.whitelist:
        text = 'fuck off'
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=text,
                                 parse_mode='markdownv2')
        return 'stop'

# print menu
def menu(buttons,
         n_cols,
         header_buttons=None,
         footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu



def printmenu(update, context):
    if auth(update, context) != 'stop':
        button_list = [
            telegram.InlineKeyboardButton("Vol -", callback_data='volume'),         # submenu
            telegram.InlineKeyboardButton("Mute", callback_data='mute'),            # button
            telegram.InlineKeyboardButton("Vol +", callback_data='volume'),         # submenu

            telegram.InlineKeyboardButton("Button 1", callback_data='btn'),       # button
            telegram.InlineKeyboardButton("Button 2", callback_data='btn'),      # button
            telegram.InlineKeyboardButton("Button 3  ", callback_data='btn'),     # button

            telegram.InlineKeyboardButton("Submenu 1", callback_data='submenu1'),  # submenu
            telegram.InlineKeyboardButton("Button 4", callback_data='btn'),    # button
            telegram.InlineKeyboardButton("Submenu 2", callback_data='submenu2'),     # submenu

            telegram.InlineKeyboardButton("Button 5", callback_data='btn'),       # button
            telegram.InlineKeyboardButton("...", callback_data='help')              # about
        ]
        text = '''
*R*                                *m e n u*
*RR*                            
*RRR*'''
        reply_markup = telegram.InlineKeyboardMarkup(menu(button_list, n_cols=3))
        try:
            query = update.callback_query
            # print(query.message.chat_id)
            context.bot.edit_message_text(chat_id=query.message.chat_id,
                                          message_id=query.message.message_id,
                                          text=text,
                                          parse_mode='markdownv2')
            context.bot.edit_message_reply_markup(chat_id=query.message.chat_id,
                                                  message_id=query.message.message_id,
                                                  text=text,
                                                  parse_mode='markdownv2',
                                                  reply_markup=reply_markup)
        except AttributeError:
            # print(update.effective_chat.id)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=text,
                                     parse_mode='markdownv2',
                                     reply_markup=reply_markup)

### volume submenu

def volumemenu(update, context):
    query = update.callback_query
    button_list = [
        telegram.InlineKeyboardButton("Vol -1%", callback_data='vm'),
        telegram.InlineKeyboardButton("Vol -3%", callback_data='vmm'),
        telegram.InlineKeyboardButton("Vol -5%", callback_data='vmmm'),
        telegram.InlineKeyboardButton("Vol -10%", callback_data='vmmmm'),
        telegram.InlineKeyboardButton("Vol -25%", callback_data='vem'),
        telegram.InlineKeyboardButton("Vol -50%", callback_data='vemm'),
        telegram.InlineKeyboardButton("Vol -75%", callback_data='vemmm'),
        telegram.InlineKeyboardButton("MUTE", callback_data='vemmmm'),
        telegram.InlineKeyboardButton("Vol +1%", callback_data='vp'),
        telegram.InlineKeyboardButton("Vol +3%", callback_data='vpp'),
        telegram.InlineKeyboardButton("Vol +5%", callback_data='vppp'),
        telegram.InlineKeyboardButton("Vol +10%", callback_data='vpppp'),
        telegram.InlineKeyboardButton("Vol +25%", callback_data='vep'),
        telegram.InlineKeyboardButton("Vol +50%", callback_data='vepp'),
        telegram.InlineKeyboardButton("Vol +75%", callback_data='veppp'),
        telegram.InlineKeyboardButton("MAX", callback_data='vepppp'),
        telegram.InlineKeyboardButton("Back", callback_data='back')
    ]
    text = '''
*R*                                *m e n u*
*RR*                             └── volume menu
*RRR*'''
    reply_markup = telegram.InlineKeyboardMarkup(menu(button_list, n_cols=4))
    context.bot.edit_message_text(chat_id=query.message.chat_id,
                                  message_id=query.message.message_id,
                                  text=text,
                                  parse_mode='markdownv2')
    context.bot.edit_message_reply_markup(chat_id=query.message.chat_id,
                                          message_id=query.message.message_id,
                                          text=text,
                                          parse_mode='markdownv2',
                                          reply_markup=reply_markup)


### volume functions
def volume(delta):
    rc = subprocess.call(['/home/dt/Scripts/home/rhq/micebot/bin/volume.sh', delta])
    return rc


def mute():
    rc = subprocess.call('/home/dt/Scripts/home/rhq/micebot/bin/mute.sh')
    return rc

### submenu example

def submenu(update, context):
    query = update.callback_query
    button_list = [
        telegram.InlineKeyboardButton("SubButton 1", callback_data='sbtn1'),
        telegram.InlineKeyboardButton("SubButton 2", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 3", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 4", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 5", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 6", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 7", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 8", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 9", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 10", callback_data='sbtn'),
        telegram.InlineKeyboardButton("SubButton 11", callback_data='sbtn'),
        telegram.InlineKeyboardButton(" ", callback_data='null'),
        telegram.InlineKeyboardButton("Back", callback_data='back')
    ]
    text = '''
*R*                                *m e n u*
*RR*                             └── sub menu
*RRR*'''
    reply_markup = telegram.InlineKeyboardMarkup(menu(button_list, n_cols=4))
    context.bot.edit_message_text(chat_id=query.message.chat_id,
                                  message_id=query.message.message_id,
                                  text=text,
                                  parse_mode='markdownv2')
    context.bot.edit_message_reply_markup(chat_id=query.message.chat_id,
                                          message_id=query.message.message_id,
                                          reply_markup=reply_markup)


# Match buttons with functions

def button(update, context):
    query = update.callback_query
    text = "u shouldnt be here, pls report me"
    if query.data == 'null':
        pass
    if query.data == 'btn' or query.data == 'sbtn' or query.data == 'sbtn1':
        pass
    if query.data == 'help':
        text = about()
    # sub menu
    if query.data == 'submenu1' or query.data =='submenu2':
        submenu(update, context)
    if query.data == 'sbtn1':
        pass
    # Volume menu
    if query.data == 'volume':
        volumemenu(update, context)
    if query.data == 'vm':
        delta = "1%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vmm':
        delta = "3%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vmmm':
        delta = "5%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vmmmm':
        delta = "10%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vem':
        delta = "25%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vemm':
        delta = "50%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vemmm':
        delta = "75%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vemmmm':
        delta = "100%-"
        volume(delta)
        text = 'Volume set ' + delta.replace('-', '\-')
    if query.data == 'vp':
        delta = "1%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'vpp':
        delta = "3%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'vppp':
        delta = "5%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'vpppp':
        delta = "10%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'vep':
        delta = "25%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'vepp':
        delta = "50%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'veppp':
        delta = "75%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'vepppp':
        delta = "100%+"
        volume(delta)
        text = 'Volume set ' + delta.replace('+', '\+')
    if query.data == 'mute':
        mute()
        text = 'Ok'
#
    if query.data == 'back':
        printmenu(update, context)

    if query.data not in ['null', 'volume', 'back', 'submenu1', 'submenu2', 'sbtn', 'sbtn1', 'btn']:
        query.edit_message_text(inline_message_id=update.callback_query.inline_message_id,
                                text=text,
                                parse_mode='markdownv2')


def main():
    start_handler = CommandHandler('start', auth)
    dispatcher.add_handler(start_handler)

    menu_handler = CommandHandler('pilot', printmenu)
    dispatcher.add_handler(menu_handler)

    dispatcher.add_handler(CallbackQueryHandler(button))

    updater.start_polling()


if __name__ == '__main__':
    main()

